﻿
$("#searchInput").keyup(function () {
    var rows = $("#toBody").find("tr").hide();

    if (this.value.length) {
        var data = this.value.split(" ");
        $.each(data, function (i, v) {
            rows.filter(":contains('" + v + "')").show();
        });
    }
    else
        rows.show();
});

$('#searchInput').DataTable( {
    language: {
        infoEmpty: "No records available - Got it?",
    }
});

$("#searchInput2").keyup(function () {
    var rows = $("#toBody2").find("tr").hide();

    if (this.value.length) {
        var data = this.value.split(" ");
        $.each(data, function (i, v) {
            rows.filter(":contains('" + v + "')").show();
        });
    }
    else
        rows.show();
});