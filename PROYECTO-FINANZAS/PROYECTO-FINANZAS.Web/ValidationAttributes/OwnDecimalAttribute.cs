﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PROYECTO_FINANZAS.Web.ValidationAttributes
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter, AllowMultiple = false)]
    public class OwnDecimalAttribute : ValidationAttribute
    {
        public OwnDecimalAttribute() { }

        public override bool IsValid(object value)
        {
            try
            {
                String Cadena = value.ToString();

                if (Cadena == "0")
                    return false;

                foreach (Char Item in Cadena)
                    if (!Char.IsNumber(Item))
                        if (!Char.IsPunctuation(Item))
                            return false;

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}