﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PROYECTO_FINANZAS.Web.ValidationAttributes
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter, AllowMultiple = false)]
    public class OnlyNumbersAttribute : ValidationAttribute
    {
        public OnlyNumbersAttribute() { }

        public override bool IsValid(object value)
        {
            try
            {
                String Cadena = value.ToString();
                
                if (!Cadena.All(X => Char.IsNumber(X)))
                    return false;

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}