﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PROYECTO_FINANZAS.Web.ValidationAttributes
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter, AllowMultiple = false)]
    public class OnlyLettersAttribute : ValidationAttribute
    {
        public OnlyLettersAttribute() { }

        public override bool IsValid(object value)
        {
            try
            {
                String Cadena = value.ToString();

                foreach(Char Item in Cadena)
                    if (!Char.IsLetter(Item))
                        if (!Char.IsWhiteSpace(Item))
                            return false;

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}