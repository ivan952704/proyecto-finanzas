﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PROYECTO_FINANZAS.Web.ValidationAttributes
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter, AllowMultiple = false)]
    public class DiasPlazoAttribute : ValidationAttribute
    {
        public DiasPlazoAttribute() { }

        public override bool IsValid(object value)
        {
            try
            {
                String Cadena = value.ToString();
                
                if (!Cadena.All(X => Char.IsNumber(X)))
                    return false;

                int N = Int32.Parse(Cadena);

                if (N < 30 || N > 10950)
                    return false;

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}