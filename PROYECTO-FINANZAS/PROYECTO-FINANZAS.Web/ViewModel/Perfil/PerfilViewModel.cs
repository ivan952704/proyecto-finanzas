﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using PROYECTO_FINANZAS.Web.ViewModel.Base;
using System.ComponentModel.DataAnnotations;
using PROYECTO_FINANZAS.Web.ValidationAttributes;

namespace PROYECTO_FINANZAS.Web.ViewModel.Perfil
{
    public class PerfilViewModel : BaseViewModel
    {
        public int? PerfilId { get; set; }

        [Required]
        [OnlyLetters(ErrorMessage = "El campo Nombre no es válido.")]
        public String Nombre { get; set; }

        [Required]
        [OnlyLetters(ErrorMessage = "El campo Apellido no es válido.")]
        public String Apellido { get; set; }

        [Required]
        [OnlyNumbers(ErrorMessage = "El campo Dni no es válido.")]
        [StringLength(8, MinimumLength = 8, ErrorMessage = "Por favor ingrese un DNI de 8 dígitos")]
        public String Dni { get; set; }

        [Required]
        [EmailAddress]
        //[UniqueEmail]
        public String Email { get; set; }

        public PerfilViewModel() { }

        public void CargarDatos(int? PerfilId)
        {
            if (PerfilId.HasValue)
            {
                Models.Perfil ObjPerfil = Context.Perfil.FirstOrDefault(X => X.PerfilId == PerfilId);

                this.PerfilId = ObjPerfil.PerfilId;
                Nombre = ObjPerfil.Nombre;
                Apellido = ObjPerfil.Apellido;
                Dni = ObjPerfil.Dni;
                Email = ObjPerfil.Email;
            }
        }
    }
}
