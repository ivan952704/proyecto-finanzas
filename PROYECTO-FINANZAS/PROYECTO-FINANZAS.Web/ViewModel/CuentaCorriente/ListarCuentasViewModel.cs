﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using PROYECTO_FINANZAS.Web.ViewModel.Base;

namespace PROYECTO_FINANZAS.Web.ViewModel.CuentaCorriente
{
    public class ListarCuentasViewModel : BaseViewModel
    {
        public List<Models.CuentaCorriente> LstCuentas;

        public ListarCuentasViewModel() { }

        public void CargarDatos(int PerfilId)
        {
            LstCuentas = Context.CuentaCorriente.OrderByDescending(X => X.NroCuenta).Where(X => X.PerfilId == PerfilId).ToList();
        }
    }
}
