﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using PROYECTO_FINANZAS.Web.ViewModel.Base;
using System.ComponentModel.DataAnnotations;
using PROYECTO_FINANZAS.Web.Helpers;
using PROYECTO_FINANZAS.Web.ValidationAttributes;

namespace PROYECTO_FINANZAS.Web.ViewModel.CuentaCorriente
{
    public class CuentaViewModel : BaseViewModel
    {
        public int? CuentaId { get; set; }

        [Required(ErrorMessage = "El campo Número de Cuenta es obligatorio.")]
        [OnlyNumbers(ErrorMessage = "El campo Número de Cuenta no es válido.")]
        [StringLength(11, MinimumLength = 11, ErrorMessage = "Por favor ingrese una Cuenta de 11 dígitos.")]
        public String NroCuenta { get; set; }

        [Required]
        public String Banco { get; set; }

        public Dictionary<String, String> DicBancos;

        public CuentaViewModel() { }

        public void CargarDatos(int? CuentaId)
        {
            DicBancos = ConstantHelpers.ObtenerBancos();
            if (CuentaId.HasValue)
            {
                Models.CuentaCorriente ObjCuenta = Context.CuentaCorriente.FirstOrDefault(X => X.CuentaCorrienteId == CuentaId);

                this.CuentaId = ObjCuenta.CuentaCorrienteId;
                NroCuenta = ObjCuenta.NroCuenta;
                Banco = ObjCuenta.Banco;
            }
        }
    }
}
