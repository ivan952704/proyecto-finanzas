﻿using PROYECTO_FINANZAS.Web.ViewModel.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PROYECTO_FINANZAS.Web.Helpers;

namespace PROYECTO_FINANZAS.Web.ViewModel.Dashboard
{
    public class DashboardViewModel : BaseViewModel
    {
        public int? totalCuentas { get; set; }
        public double? totalDinero { get; set; }
        public int? totalPerfiles { get; set; }

        public void CargarDatos(int UsuarioId, int PerfilId)
        {
            totalCuentas = Context.Perfil.Where(X => X.PerfilId == PerfilId).Count();
            totalPerfiles = Context.Perfil.Where(X => X.UsuarioId == UsuarioId).Count();
        }
        public decimal ObtenerGananciaTotalHoy(int PerfilId)
        {
            decimal total = 0;
            Deposito.ListarDepositosViewModel model = new Deposito.ListarDepositosViewModel();
            List<Models.CuentaCorriente> cuentas = Context.Perfil.Where(X => X.PerfilId == PerfilId).First().CuentaCorriente.ToList();
            foreach (Models.CuentaCorriente cuenta in cuentas)
            {
                List<Models.Deposito> depositos = cuenta.Deposito.ToList();
                model.LstDepositos = depositos;
                total += model.ObtenerTotalDeIngresosAlRetirarHoy();
            }
            return total;
        }
        public decimal ObtenerDineroInvertido(int PerfilId)
        {
            decimal total = 0;
            Deposito.ListarDepositosViewModel model = new Deposito.ListarDepositosViewModel();
            List<Models.CuentaCorriente> cuentas = Context.Perfil.Where(X => X.PerfilId == PerfilId).First().CuentaCorriente.ToList();
            foreach (Models.CuentaCorriente cuenta in cuentas)
            {
                List<Models.Deposito> depositos = cuenta.Deposito.ToList();
                model.LstDepositos = depositos;
                total += model.LstDepositos.Sum(x => x.MontoDepositado);
            }
            return total;

        }
    }
}
