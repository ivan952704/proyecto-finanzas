﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using PROYECTO_FINANZAS.Web.ViewModel.Base;

namespace PROYECTO_FINANZAS.Web.ViewModel.Deposito
{
    public class ListarDepositosViewModel : BaseViewModel
    {
        public List<Models.Deposito> LstDepositos;
        public int CuentaId { get; set; }
        public decimal ITF = 0.00005m;

        public ListarDepositosViewModel() { }

        public void CargarDatos(int CuentaId)
        {
            this.CuentaId = CuentaId;
            LstDepositos = Context.Deposito.OrderByDescending(X => X.DepositoId).Where(X => X.CuentaCorrienteId == CuentaId).ToList();
        }
        public DateTime ObtenerFechaDentroDeRango(int numeroDeposito)
        {
            Models.Deposito deposito = LstDepositos[numeroDeposito];
            TimeSpan? diferencia = deposito.FechaVencimiento - DateTime.Today;
            int diferenciaEnDias = diferencia.Value.Days;
            if (DateTime.Today >= deposito.FechaDeposito && DateTime.Today <= deposito.FechaVencimiento)
            {
                return DateTime.Today;
            }
            else
            {
                if (DateTime.Today < deposito.FechaDeposito)
                {
                    return deposito.FechaDeposito;
                }
                else
                {
                    return deposito.FechaVencimiento.Value;
                }
            }
        }

        public int ObtenerDiasFaltantes(int numeroDeposito)
        {
            Models.Deposito deposito = LstDepositos[numeroDeposito];
            TimeSpan? diferencia = deposito.FechaVencimiento - ObtenerFechaDentroDeRango(numeroDeposito);
            return diferencia.Value.Days;
        }
        public int ObtenerDiasTranscurridos(int numeroDeposito)
        {
            Models.Deposito deposito = LstDepositos[numeroDeposito];
            TimeSpan? diferencia = ObtenerFechaDentroDeRango(numeroDeposito) - deposito.FechaDeposito;
            return diferencia.Value.Days;
        }
        public Models.Deposito obtenerDeposito(int numeroDeposito)
        {
            return LstDepositos[numeroDeposito];
        }
        public decimal AplicarTEAHoy(int numeroDeposito, bool penalizado)
        {
            Models.Deposito deposito = obtenerDeposito(numeroDeposito);
            decimal montoDepositado = deposito.MontoDepositado;
            double diasEnPeriodo = 360;
            double penalizacion = (double)deposito.Penalizacion;
            double tea = (double)deposito.TasaEfectiva;
            double diasTranscurridos = ObtenerDiasTranscurridos(numeroDeposito);
            decimal dinero = 0;
            dinero = montoDepositado * (decimal)Math.Pow((1 + (penalizado ? penalizacion : tea)), diasTranscurridos / diasEnPeriodo);
            return dinero;
        }
        public decimal AplicarTEAPeriodo(int numeroDeposito, bool penalizado)
        {
            Models.Deposito deposito = obtenerDeposito(numeroDeposito);
            decimal montoDepositado = deposito.MontoDepositado;
            double diasEnPeriodo = 360;
            double penalizacion = (double)deposito.Penalizacion;
            double tea = (double)deposito.TasaEfectiva;
            double diasTranscurridos = (obtenerDeposito(numeroDeposito).FechaVencimiento - obtenerDeposito(numeroDeposito).FechaDeposito).Value.Days;
            decimal dinero = 0;
            dinero = montoDepositado * (decimal)Math.Pow((1 + (penalizado ? penalizacion : tea)), diasTranscurridos / diasEnPeriodo);
            return dinero;
        }

        public decimal DineroGanadoHoy(int numeroDeposito, bool penalizado)
        {
            return (AplicarTEAHoy(numeroDeposito, penalizado) - obtenerDeposito(numeroDeposito).MontoDepositado);
        }

        public bool EstaPenalizado(int numeroDeposito)
        {
            if (ObtenerFechaDentroDeRango(numeroDeposito) == obtenerDeposito(numeroDeposito).FechaVencimiento)
            {
                return false;
            }
            return true;
        }
        public decimal DineroGanadoPeriodo(int numeroDeposito, bool penalizado)
        {
            return AplicarTEAPeriodo(numeroDeposito, penalizado) - obtenerDeposito(numeroDeposito).MontoDepositado;
        }

        public int ObtenerDiferenciaDeDias(int numeroDeposito)
        {
            Models.Deposito deposito = obtenerDeposito(numeroDeposito);
            return (deposito.FechaVencimiento - deposito.FechaDeposito).Value.Days;
        }
        public decimal ObtenerTotalDeIngresosAlRetirarHoy()
        {
            decimal ingresos = 0;
            for (int i = 0; i < LstDepositos.Count; i++)
            {
                ingresos += DineroGanadoHoy(i, EstaPenalizado(i));
            }
            return ingresos - (ingresos * ITF);

        }

        public decimal ObtenerTotalDeIngresosPeriodo()
        {
            decimal ingresos = 0;
            for (int i = 0; i < LstDepositos.Count; i++)
            {
                ingresos += DineroGanadoPeriodo(i, false);
            }
            return ingresos - (ingresos * ITF);
        }

        public decimal ObtenerDineroActualmenteEnCuentas()
        {
            decimal ingresos = 0;
            for (int i = 0; i< LstDepositos.Count; i++)
            {
                ingresos += AplicarTEAHoy(i, false);
            }
            return ingresos;
        }
    }
}
