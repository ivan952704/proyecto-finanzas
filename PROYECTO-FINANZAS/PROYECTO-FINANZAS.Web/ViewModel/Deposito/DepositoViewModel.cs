﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using PROYECTO_FINANZAS.Web.ViewModel.Base;
using System.ComponentModel.DataAnnotations;
using PROYECTO_FINANZAS.Web.ValidationAttributes;
using PROYECTO_FINANZAS.Web.Helpers;

namespace PROYECTO_FINANZAS.Web.ViewModel.Deposito
{
    public class DepositoViewModel : BaseViewModel
    {
        public int? DepositoId { get; set; }

        public int CuentaId { get; set; }

        [Required]
        [OwnDecimal(ErrorMessage = "El campo Valor de Tasa no es válido.")]
        public Decimal ValorTasa { get; set; }

        [Required]
        [OwnDecimal(ErrorMessage = "El campo Monto Depositado no es válido.")]
        public Decimal MontoDepositado { get; set; }

        [Required]
        [DiasPlazo(ErrorMessage = "El campo Número de Días de Plazo Fijo debe ser mayor o igual a 30.")]
        public int NroDiasPlazoFijo { get; set; }

        //[Required]
        public DateTime FechaDeposito { get; set; }

        [Required]
        [OwnDecimal(ErrorMessage = "El campo Penalización no es válido.")]
        public Decimal Penalizacion { get; set; }

        [Required(ErrorMessage = "El campo Tipo de Tasa es obligatorio.")]
        public String Tasa { get; set; }

        public Dictionary<String, String> DicTasas;

        public DepositoViewModel() { }

        public void CargarDatos(int CuentaId, int? DepositoId)
        {
            this.CuentaId = CuentaId;
            FechaDeposito = DateTime.Now;
            DicTasas = ConstantHelpers.ObtenerTasas();

            if (DepositoId.HasValue)
            {
                Models.Deposito ObjDeposito = Context.Deposito.FirstOrDefault(X => X.DepositoId == DepositoId);

                this.DepositoId = ObjDeposito.DepositoId;
                ValorTasa = ObjDeposito.TasaEfectiva;
                MontoDepositado = ObjDeposito.MontoDepositado;
                NroDiasPlazoFijo = ObjDeposito.NroDiasPlazoFijo;
                FechaDeposito = ObjDeposito.FechaDeposito;
                Penalizacion = ObjDeposito.Penalizacion;
            }
        }
    }
}
