﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PROYECTO_FINANZAS.Web.Helpers
{
    public static class ExtensionHelpers
    {
        public static Int32 ToInteger(this object valor)
        {
            return Convert.ToInt32(valor);
        }

        public static Int32 GetUsuarioId(this HttpSessionStateBase session)
        {
            return session["UsuarioId"].ToInteger();
        }

        public static String GetNombreUsuario(this HttpSessionStateBase session)
        {
            return session["NombreUsuario"].ToString();
        }
        public static Int32 GetPerfilId(this HttpSessionStateBase session)
        {
            return session["PerfilId"].ToInteger();
        }
        public static String GetNombrePerfil(this HttpSessionStateBase session)
        {
            return session["NombrePerfil"].ToString();
        }
    }
}
