﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PROYECTO_FINANZAS.Web.Helpers
{
    public static class ConstantHelpers
    {
        public static Dictionary<String, String> ObtenerTasas()
        {
            return new Dictionary<String, String>
            {
                {"TEA", "Tasa Efectiva Anual"},
                {"TNA", "Tasa Nominal Anual (c.d)"}
            };
        }

        public static Dictionary<String, String> ObtenerBancos()
        {
            return new Dictionary<String, String>
            {
                {"SCOTIABANK", "Scotiabank"},
                {"INTERBANK", "Interbank"},
                {"BCP", "BCP"},
            };
        }
    }
}
