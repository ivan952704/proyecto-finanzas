﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using PROYECTO_FINANZAS.Web.Models;

namespace PROYECTO_FINANZAS.Web.Controllers
{
    public class BaseController : Controller
    {
        protected FinanzasEntities Context;

        public BaseController()
        {
            Context = new FinanzasEntities();
        }
    }
}