﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using PROYECTO_FINANZAS.Web.ViewModel.Perfil;
using System.Transactions;
using PROYECTO_FINANZAS.Web.Models;
using PROYECTO_FINANZAS.Web.Helpers;
using PROYECTO_FINANZAS.Web.Filters;

namespace PROYECTO_FINANZAS.Web.Controllers
{
    public class PerfilController : BaseController
    {
        [RolAuthFilter()]
        public ActionResult SeleccionarPerfil(int? perfilId)
        {
            var ObjUsuario = Context.Perfil.FirstOrDefault(X => X.PerfilId == perfilId);
            Session["PerfilId"] = ObjUsuario.PerfilId;
            Session["NombrePerfil"] = ObjUsuario.Nombre;
            TempData["Mensaje"] = "¡Perfil seleccionado exitosamente! Bienvenido " + ObjUsuario.Nombre;
            return RedirectToAction("ListarPerfiles");
        }

        [RolAuthFilter()]
        public ActionResult ListarPerfiles()
        {
            ListarPerfilesViewModel ViewModel = new ListarPerfilesViewModel();
            ViewModel.CargarDatos(Session.GetUsuarioId());
            return View(ViewModel);
        }

        [RolAuthFilter()]
        public ActionResult Delete(int? PerfilId)
        {
            try
            {
                int UsuarioId = Session.GetUsuarioId();
                int ContPerfiles = Context.Perfil.Where(X => X.UsuarioId == UsuarioId).Count();
                if (ContPerfiles == 1)
                {
                    TempData["Mensaje"] = "Debe tener al menos un perfil en su cuenta";
                    return RedirectToAction("ListarPerfiles");
                }

                using (var tc = new TransactionScope())
                {
                    if (PerfilId.HasValue)
                    {
                        Perfil ObjPerfil = Context.Perfil.FirstOrDefault(X => X.PerfilId == PerfilId);
                        Context.Perfil.Remove(ObjPerfil);
                        Context.SaveChanges();
                        TempData["Mensaje"] = "El perfil fue eliminado exitosamente";
                        tc.Complete();
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["Mensaje"] = "Error al eliminar el perfil, vuelva a intentarlo.";
            }
            return RedirectToAction("ListarPerfiles");
        }

        [RolAuthFilter()]
        public ActionResult Edit(int? PerfilId)
        {
            PerfilViewModel ViewModel = new PerfilViewModel();
            ViewModel.CargarDatos(PerfilId);
            return View(ViewModel);
        }

        [HttpPost]
        public ActionResult Edit(PerfilViewModel ViewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    ViewModel.CargarDatos(ViewModel.PerfilId);
                    TryUpdateModel(ViewModel);

                    TempData["Mensaje"] = "Por favor, complete los campos requeridos";
                    return View(ViewModel);
                }

                using (var tc = new TransactionScope())
                {
                    Perfil ObjPerfil = null;

                    if (ViewModel.PerfilId.HasValue)
                        ObjPerfil = Context.Perfil.FirstOrDefault(X => X.PerfilId == ViewModel.PerfilId);
                    else
                    {
                        ObjPerfil = new Perfil();
                        ObjPerfil.UsuarioId = Session.GetUsuarioId();
                        Context.Perfil.Add(ObjPerfil);
                    }

                    ObjPerfil.Nombre = ViewModel.Nombre.ToUpper();
                    ObjPerfil.Apellido = ViewModel.Apellido.ToUpper();
                    ObjPerfil.Dni = ViewModel.Dni;
                    ObjPerfil.Email = ViewModel.Email;

                    Context.SaveChanges();
                    TempData["Mensaje"] = "El Perfil fue procesado exitosamente";
                    tc.Complete();
                }
            }
            catch (Exception ex)
            {
                ViewModel.CargarDatos(ViewModel.PerfilId);
                TryUpdateModel(ViewModel);

                TempData["Mensaje"] = "Por favor, complete los campos requeridos";
                return View(ViewModel);
            }
            return RedirectToAction("ListarPerfiles");
        }
    }
}
