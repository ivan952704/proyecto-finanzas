﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using PROYECTO_FINANZAS.Web.Filters;
using PROYECTO_FINANZAS.Web.Helpers;
using PROYECTO_FINANZAS.Web.ViewModel.CuentaCorriente;
using System.Transactions;

namespace PROYECTO_FINANZAS.Web.Controllers
{
    public class CuentaCorrienteController : BaseController
    {
        [RolAuthFilter()]
        public ActionResult ListarCuentas()
        {
            ListarCuentasViewModel ViewModel = new ListarCuentasViewModel();
            ViewModel.CargarDatos(Session.GetPerfilId());
            return View(ViewModel);
        }

        [RolAuthFilter()]
        public ActionResult Delete(int? CuentaId)
        {
            try
            {
                using (var tc = new TransactionScope())
                {
                    if (CuentaId.HasValue)
                    {
                        Models.CuentaCorriente ObjCuenta = Context.CuentaCorriente.FirstOrDefault(X => X.CuentaCorrienteId == CuentaId);
                        Context.Deposito.RemoveRange(ObjCuenta.Deposito);
                        Context.CuentaCorriente.Remove(ObjCuenta);
                        Context.SaveChanges();
                        TempData["Mensaje"] = "La cuenta corriente fue eliminada exitosamente";
                        tc.Complete();
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["Mensaje"] = "Error al eliminar la cuenta corriente, vuelva a intentarlo.";
            }
            return RedirectToAction("ListarCuentas");
        }

        [RolAuthFilter()]
        public ActionResult Edit(int? CuentaId)
        {
            CuentaViewModel ViewModel = new CuentaViewModel();
            ViewModel.CargarDatos(CuentaId);
            return View(ViewModel);
        }

        [HttpPost]
        public ActionResult Edit(CuentaViewModel ViewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    ViewModel.CargarDatos(ViewModel.CuentaId);
                    TryUpdateModel(ViewModel);

                    TempData["Mensaje"] = "Por favor, complete los campos requeridos";
                    return View(ViewModel);
                }

                using (var tc = new TransactionScope())
                {
                    Models.CuentaCorriente ObjCuenta = null;

                    if (ViewModel.CuentaId.HasValue)
                        ObjCuenta = Context.CuentaCorriente.FirstOrDefault(X => X.CuentaCorrienteId == ViewModel.CuentaId);
                    else
                    {
                        ObjCuenta = new Models.CuentaCorriente();
                        ObjCuenta.PerfilId = Session.GetPerfilId();
                        Context.CuentaCorriente.Add(ObjCuenta);
                    }

                    ObjCuenta.NroCuenta = ViewModel.NroCuenta;
                    ObjCuenta.Banco = ViewModel.Banco;

                    Context.SaveChanges();
                    TempData["Mensaje"] = "La cuenta corriente fue procesada exitosamente";
                    tc.Complete();
                }
            }
            catch (Exception ex)
            {
                ViewModel.CargarDatos(ViewModel.CuentaId);
                TryUpdateModel(ViewModel);

                TempData["Mensaje"] = "Por favor, complete los campos requeridos";
                return View(ViewModel);
            }
            return RedirectToAction("ListarCuentas");
        }
    }
}