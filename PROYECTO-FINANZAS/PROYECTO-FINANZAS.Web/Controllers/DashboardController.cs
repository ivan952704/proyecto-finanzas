﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using PROYECTO_FINANZAS.Web.Filters;
using PROYECTO_FINANZAS.Web.Helpers;
using PROYECTO_FINANZAS.Web.ViewModel.Dashboard;

namespace PROYECTO_FINANZAS.Web.Controllers
{
    public class DashboardController : BaseController
    {
        [RolAuthFilter()]
        public ActionResult Index()
        {
            DashboardViewModel viewModel = new DashboardViewModel();
            viewModel.CargarDatos(Session.GetUsuarioId(), Session.GetPerfilId());
            return View(viewModel);
        }
    }
}