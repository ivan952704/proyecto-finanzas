﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using PROYECTO_FINANZAS.Web.Filters;
using PROYECTO_FINANZAS.Web.ViewModel.Deposito;
using System.Transactions;

namespace PROYECTO_FINANZAS.Web.Controllers
{
    public class DepositoController : BaseController
    {
        [RolAuthFilter()]
        public ActionResult ListarDepositos(int CuentaId)
        {
            ListarDepositosViewModel ViewModel = new ListarDepositosViewModel();
            ViewModel.CargarDatos(CuentaId);
            return View(ViewModel);
        }

        [RolAuthFilter()]
        public ActionResult Eliminar(int? DepositoId)
        {
            int AuxCuentaId = 0;
            try
            {
                using (var tc = new TransactionScope())
                {
                    if (DepositoId.HasValue)
                    {
                        Models.Deposito ObjDeposito = Context.Deposito.FirstOrDefault(X => X.DepositoId == DepositoId);
                        AuxCuentaId = ObjDeposito.CuentaCorrienteId;
                        Context.Deposito.Remove(ObjDeposito);
                        Context.SaveChanges();
                        TempData["Mensaje"] = "El deposito fue eliminado exitosamente";
                        tc.Complete();
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["Mensaje"] = "Error al eliminar el deposito, vuelva a intentarlo.";
            }
            return RedirectToAction("ListarDepositos", new { CuentaId = AuxCuentaId });
        }

        [RolAuthFilter()]
        public ActionResult Edit(int CuentaId, int? DepositoId)
        {
            DepositoViewModel ViewModel = new DepositoViewModel();
            ViewModel.CargarDatos(CuentaId, DepositoId);
            return View(ViewModel);
        }

        [HttpPost]
        public ActionResult Edit(DepositoViewModel ViewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    ViewModel.CargarDatos(ViewModel.CuentaId, ViewModel.DepositoId);
                    TryUpdateModel(ViewModel);

                    TempData["Mensaje"] = "Por favor, complete los campos requeridos";
                    return View(ViewModel);
                }

                using (var tc = new TransactionScope())
                {
                    Models.Deposito ObjDeposito = null;

                    if (ViewModel.DepositoId.HasValue)
                        ObjDeposito = Context.Deposito.FirstOrDefault(X => X.DepositoId == ViewModel.DepositoId);
                    else
                    {
                        ObjDeposito = new Models.Deposito();
                        ObjDeposito.CuentaCorrienteId = ViewModel.CuentaId;
                        Context.Deposito.Add(ObjDeposito);
                    }

                    if(ViewModel.Tasa == "TNA")
                    {
                        Double Aux = 1 + (Double)ViewModel.ValorTasa / 360.0;
                        ObjDeposito.TasaEfectiva = (decimal)(Math.Pow(Aux, 360.0) - 1.0);
                    }
                    else if (ViewModel.Tasa == "TEA")
                    {
                        ObjDeposito.TasaEfectiva = ViewModel.ValorTasa;
                    }

                    ObjDeposito.Penalizacion = ViewModel.Penalizacion;
                    ObjDeposito.FechaDeposito = ViewModel.FechaDeposito;
                    ObjDeposito.MontoDepositado = ViewModel.MontoDepositado;
                    ObjDeposito.NroDiasPlazoFijo = ViewModel.NroDiasPlazoFijo;
                    ObjDeposito.FechaVencimiento = ViewModel.FechaDeposito.AddDays(ViewModel.NroDiasPlazoFijo);

                    Context.SaveChanges();
                    TempData["Mensaje"] = "El Deposito fue procesado exitosamente";
                    tc.Complete();
                }
            }
            catch (Exception ex)
            {
                ViewModel.CargarDatos(ViewModel.CuentaId, ViewModel.DepositoId);
                TryUpdateModel(ViewModel);

                TempData["Mensaje"] = "Por favor, complete los campos requeridos";
                return View(ViewModel);
            }
            return RedirectToAction("ListarDepositos", new { CuentaId = ViewModel.CuentaId});
        }
    }
}
