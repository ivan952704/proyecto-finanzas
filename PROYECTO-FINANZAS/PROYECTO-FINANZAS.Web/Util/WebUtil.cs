﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using PROYECTO_FINANZAS.Web.Models;

namespace PROYECTO_FINANZAS.Web.Util
{
    public static class WebUtil
    {
        public static bool esDniRepetido(String Dni)
        {
            FinanzasEntities Context = new FinanzasEntities();
            return Context.Perfil.Any(X => X.Dni == Dni);
        }

        public static bool esUserNameRepetido(String Username)
        {
            FinanzasEntities Context = new FinanzasEntities();
            return Context.Usuario.Any(X => X.NombreUsuario == Username);
        }

        public static bool esEmailRepetido(String Email)
        {
            FinanzasEntities Context = new FinanzasEntities();
            return Context.Perfil.Any(X => X.Email == Email);
        }
    }
}
