USE [DBFINANZAS]
GO
/****** Object:  Table [dbo].[CuentaCorriente]    Script Date: 28/11/2015 12:47:51 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CuentaCorriente](
	[CuentaCorrienteId] [int] IDENTITY(1,1) NOT NULL,
	[NroCuenta] [char](11) NOT NULL,
	[Banco] [varchar](20) NOT NULL,
	[PerfilId] [int] NOT NULL,
 CONSTRAINT [PK_CuentaCorriente] PRIMARY KEY CLUSTERED 
(
	[CuentaCorrienteId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Deposito]    Script Date: 28/11/2015 12:47:52 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Deposito](
	[DepositoId] [int] IDENTITY(1,1) NOT NULL,
	[MontoDepositado] [decimal](18, 2) NOT NULL,
	[NroDiasPlazoFijo] [int] NOT NULL,
	[FechaDeposito] [date] NOT NULL,
	[FechaCancelacion] [date] NULL,
	[FechaVencimiento] [date] NULL,
	[Penalizacion] [decimal](18, 2) NOT NULL,
	[CuentaCorrienteId] [int] NOT NULL,
	[TasaEfectiva] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_Deposito] PRIMARY KEY CLUSTERED 
(
	[DepositoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Perfil]    Script Date: 28/11/2015 12:47:52 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Perfil](
	[PerfilId] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](40) NOT NULL,
	[Apellido] [varchar](40) NOT NULL,
	[Email] [varchar](40) NOT NULL,
	[Dni] [char](8) NOT NULL,
	[UsuarioId] [int] NOT NULL,
 CONSTRAINT [PK_Perfil] PRIMARY KEY CLUSTERED 
(
	[PerfilId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Usuario]    Script Date: 28/11/2015 12:47:52 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Usuario](
	[UsuarioId] [int] IDENTITY(1,1) NOT NULL,
	[NombreUsuario] [varchar](20) NOT NULL,
	[Password] [varchar](20) NOT NULL,
	[Bloqueado] [bit] NOT NULL,
 CONSTRAINT [PK_Usuario] PRIMARY KEY CLUSTERED 
(
	[UsuarioId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[CuentaCorriente] ON 

INSERT [dbo].[CuentaCorriente] ([CuentaCorrienteId], [NroCuenta], [Banco], [PerfilId]) VALUES (3, N'34534567567', N'SCOTIABANK', 1)
INSERT [dbo].[CuentaCorriente] ([CuentaCorrienteId], [NroCuenta], [Banco], [PerfilId]) VALUES (4, N'45634576456', N'INTERBANK', 1)
SET IDENTITY_INSERT [dbo].[CuentaCorriente] OFF
SET IDENTITY_INSERT [dbo].[Deposito] ON 

INSERT [dbo].[Deposito] ([DepositoId], [MontoDepositado], [NroDiasPlazoFijo], [FechaDeposito], [FechaCancelacion], [FechaVencimiento], [Penalizacion], [CuentaCorrienteId], [TasaEfectiva]) VALUES (13, CAST(200.00 AS Decimal(18, 2)), 200, CAST(N'2015-09-27' AS Date), NULL, CAST(N'2016-04-14' AS Date), CAST(0.08 AS Decimal(18, 2)), 3, CAST(0.05 AS Decimal(18, 2)))
INSERT [dbo].[Deposito] ([DepositoId], [MontoDepositado], [NroDiasPlazoFijo], [FechaDeposito], [FechaCancelacion], [FechaVencimiento], [Penalizacion], [CuentaCorrienteId], [TasaEfectiva]) VALUES (14, CAST(200000.00 AS Decimal(18, 2)), 30, CAST(N'2015-10-27' AS Date), NULL, CAST(N'2015-11-26' AS Date), CAST(0.02 AS Decimal(18, 2)), 3, CAST(0.05 AS Decimal(18, 2)))
SET IDENTITY_INSERT [dbo].[Deposito] OFF
SET IDENTITY_INSERT [dbo].[Perfil] ON 

INSERT [dbo].[Perfil] ([PerfilId], [Nombre], [Apellido], [Email], [Dni], [UsuarioId]) VALUES (1, N'Juan', N'Perez', N'perez@gmail.com', N'34564567', 1)
INSERT [dbo].[Perfil] ([PerfilId], [Nombre], [Apellido], [Email], [Dni], [UsuarioId]) VALUES (2, N'Ivan', N'Contreras', N'ivan@gmail.com', N'45345678', 2)
INSERT [dbo].[Perfil] ([PerfilId], [Nombre], [Apellido], [Email], [Dni], [UsuarioId]) VALUES (3, N'IVAN', N'CONTRERAS', N'ivan2@gmail.com', N'223     ', 1)
SET IDENTITY_INSERT [dbo].[Perfil] OFF
SET IDENTITY_INSERT [dbo].[Usuario] ON 

INSERT [dbo].[Usuario] ([UsuarioId], [NombreUsuario], [Password], [Bloqueado]) VALUES (1, N'root', N'root', 0)
INSERT [dbo].[Usuario] ([UsuarioId], [NombreUsuario], [Password], [Bloqueado]) VALUES (2, N'admin', N'admin', 0)
SET IDENTITY_INSERT [dbo].[Usuario] OFF
ALTER TABLE [dbo].[CuentaCorriente]  WITH CHECK ADD  CONSTRAINT [FK_CuentaCorriente_Perfil] FOREIGN KEY([PerfilId])
REFERENCES [dbo].[Perfil] ([PerfilId])
GO
ALTER TABLE [dbo].[CuentaCorriente] CHECK CONSTRAINT [FK_CuentaCorriente_Perfil]
GO
ALTER TABLE [dbo].[Deposito]  WITH CHECK ADD  CONSTRAINT [FK_Deposito_CuentaCorriente] FOREIGN KEY([CuentaCorrienteId])
REFERENCES [dbo].[CuentaCorriente] ([CuentaCorrienteId])
GO
ALTER TABLE [dbo].[Deposito] CHECK CONSTRAINT [FK_Deposito_CuentaCorriente]
GO
ALTER TABLE [dbo].[Perfil]  WITH CHECK ADD  CONSTRAINT [FK_Perfil_Usuario] FOREIGN KEY([UsuarioId])
REFERENCES [dbo].[Usuario] ([UsuarioId])
GO
ALTER TABLE [dbo].[Perfil] CHECK CONSTRAINT [FK_Perfil_Usuario]
GO
